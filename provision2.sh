#!/bin/bash
#Скрипт обработчик запроса в AWS для создания hosts файла ansible
# $1 $CI_PROJECT_DIR
# $2 $CIVMNAME
# $3 $ANS_DIR
if [ $(uname) == "Darwin" ] ;
then 
sed -i '' -e 's/\,//g' $1/$2_vm_ip.txt
sed -i '' -e 's/\"//g' $1/$2_vm_ip.txt
sed -i '' -e 's/\ //g' $1/$2_vm_ip.txt
sed -i '' -e 's/\^$//g' $1/$2_vm_ip.txt
awk '{ print FNR " " $0 }' $1/$2_vm_ip.txt > $1/$3/hosts
sed -i '' -e 's| | ansible_host=|' $1/$3/hosts
sed -i '' -e 's|^|'"$2"'_server|' $1/$3/hosts
sed -i '' -e 's|$| ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/test.pem|' $1/$3/hosts
sed -i '' -e 's/^/['"$2"'_servers]\n/' $1/$3/hosts
sed -i '' -e 's/app_servers/'"$2"'_servers/' $1/$3/main.yml
else
sed -i -e 's/\,//g' -e 's/\"//g' -e 's/\ //g' -e s/\^$//g $1/$2_vm_ip.txt
awk '{ print FNR " " $0 }' $1/$2_vm_ip.txt > $1/$3/hosts
sed -i -e 's| | ansible_host=|' -e 's|^|'"$2"'_server|' -e 's|$| ansible_user=ubuntu ansible_ssh_private_key_file=~/.ssh/test.pem|' -e '1 i['"$2"'_servers]' $1/$3/hosts
sed -i 's/app_servers/'"$2"'_servers/' $1/$3/main.yml
fi
