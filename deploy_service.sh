#!/bin/bash
#CIVMNAME=$1
#tag=$2
set -x
while IFS= read -r line; do
if
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo docker pull tonic153/app:'$2' && sudo docker rm -f webapp && sudo docker run --name webapp -d -it -p8080:8080 tonic153/app:'$2'  && exit ";
then echo "App deployed on instance $line";
else echo "App NOT deployed on instance '$line'" & exit 1;
fi < /dev/null;
done < $1_vm_ip.txt
