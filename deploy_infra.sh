#!/bin/bash
#CIVMNAME=$1
#SSH_CONN="ssh -i "~/.ssh/test.pem" ubuntu@'$line'"
set -x
while IFS= read -r line; do
if
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo apt-get update; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo apt-get install ca-certificates curl; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo install -m 0755 -d /etc/apt/keyrings; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo chmod a+r /etc/apt/keyrings/docker.asc; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo echo "deb [arch='$(dpkg --print-architecture)' signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu '$(. /etc/os-release && echo "$VERSION_CODENAME")' stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo apt-get update; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "yes | sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin; exit" || exit 1
  ssh -i "~/.ssh/test.pem" ubuntu@$line "sudo docker run hello-world; exit" || exit 1
then echo "Infra deployed on instance $line";
else echo "App NOT deployed on instance $line" & exit 1;
fi < /dev/null;
done < $1_vm_ip.txt
